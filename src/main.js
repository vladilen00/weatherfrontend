import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueAlertify from 'vue-alertify'
import BootstrapVue from 'bootstrap-vue'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faThermometerThreeQuarters, faWind, faWater, faSpinner } from '@fortawesome/free-solid-svg-icons'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '@trevoreyre/autocomplete-vue/dist/style.css'

library.add(faThermometerThreeQuarters, faWind, faWater, faSpinner)

Vue.config.productionTip = false

Vue.use(VueAlertify)
Vue.use(BootstrapVue)
Vue.use(VueAxios, axios)
Vue.use(require('vue-moment'));
Vue.component('font-awesome-icon', FontAwesomeIcon)

/**
 * Подключение и настройка компонента axios.
 */
Vue.axios.defaults.baseURL = process.env.VUE_APP_BASE_URL

/**
 * Инициализация Vue приложения
 *
 * @type {object}
 */
new Vue({ render: h => h(App) }).$mount('#app')
