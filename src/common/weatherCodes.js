/**
 * Список кодов для состояния текущей погоды.
 */
export default {
    'clear':                                'Ясно',
    'partly-cloudy':                        'Малооблачно',
    'cloudy':                               'Облачно с прояснениями',
    'overcast':                             'Пасмурно',
    'partly-cloudy-and-light-rain':         'Небольшой дождь',
    'partly-cloudy-and-rain':               'Дождь',
    'overcast-and-rain':                    'Сильный дождь',
    'overcast-thunderstorms-with-rain':     'Сильный дождь, гроза',
    'cloudy-and-light-rain':                'Небольшой дождь',
    'overcast-and-light-rain':              'Небольшой дождь',
    'cloudy-and-rain':                      'Дождь',
    'overcast-and-wet-snow':                'Дождь со снегом',
    'partly-cloudy-and-light-snow':         'Небольшой снег',
    'partly-cloudy-and-snow':               'Снег',
    'overcast-and-snow':                    'Снегопад',
    'cloudy-and-light-snow':                'Небольшой снег',
    'overcast-and-light-snow':              'Небольшой снег',
    'cloudy-and-snow':                      'Снег'
}